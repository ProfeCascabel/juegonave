<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.3" name="objetos" tilewidth="128" tileheight="128" tilecount="128" columns="8">
 <image source="spritesheet_tiles.png" width="1024" height="2048"/>
 <tile id="32">
  <properties>
   <property name="name" value="gas"/>
  </properties>
 </tile>
 <tile id="50">
  <properties>
   <property name="name" value="mosca"/>
  </properties>
 </tile>
 <tile id="65">
  <properties>
   <property name="name" value="boton"/>
  </properties>
 </tile>
 <tile id="81">
  <properties>
   <property name="name" value="moco"/>
  </properties>
 </tile>
 <tile id="82">
  <properties>
   <property name="name" value="barrera"/>
  </properties>
 </tile>
 <tile id="113">
  <properties>
   <property name="name" value="plataforma"/>
  </properties>
 </tile>
 <tile id="115">
  <properties>
   <property name="name" value="rana"/>
  </properties>
 </tile>
</tileset>
