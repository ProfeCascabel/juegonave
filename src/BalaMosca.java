import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class BalaMosca extends BaseActor{
	protected LevelScreen nivel;
	protected float tiempoBala;
	protected float cuentaTiempo;
	protected float velocidadBala;
	protected int direccion;
	protected boolean enabled;
	public BalaMosca(float x, float y, Stage s, LevelScreen nivel) {
		super(x, y, s);
		// TODO Auto-generated constructor stub
		loadTexture("assets/items/fireball.png");
		tiempoBala=10;
		this.nivel=nivel;
		velocidadBala=800;
		enabled=false;
	}

	
	
	@Override
	public void act(float dt) {
		// TODO Auto-generated method stub
	
		
		if(enabled) {super.act(dt);
		this.rotateBy(-10*direccion);
		moveBy(velocityVec.x*dt, velocityVec.y*dt);
		
		
			if( this.enabled && this.overlaps(nivel.jugador)) {
				nivel.jugador.dano(10);
				this.enabled=false;
				
			}
		
		
		cuentaTiempo-=dt;
		
		
		if(cuentaTiempo<=0) {
			enabled=false;
		}
		
		}
	}


	@Override
	public void draw(Batch batch, float parentAlpha) {
		// TODO Auto-generated method stub
		if (enabled) super.draw(batch, parentAlpha);
	}


	public void disparar(float x, float y, float posx, float posy) {
		
		this.enabled=true;
		
		this.setPosition(posx, posy);
		
		cuentaTiempo=tiempoBala;
		this.velocityVec.set(velocidadBala*x, velocidadBala*y);
		this.direccion=direccion;
		if(posx<0) {direccion=-1;}
		else {direccion=1;}
	}

}
