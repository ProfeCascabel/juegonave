import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class Enemigo extends BaseActor{
	//Clase de la que heredar�n los distintos enemigos del juego. Al tener un padre com�n, podremos darles un trato
	//conjunto cuando convenga.
	
	public boolean vivo;//flag que indica si el personaje est� vivo o no, solo lo pintaremos y actualizaremos si est� vivo
	protected LevelScreen nivel;//referencia al nivel
	
	public Enemigo(float x, float y, Stage s, LevelScreen nivel) {
		super(x, y, s);
		// TODO Auto-generated constructor stub
		this.nivel=nivel;
		vivo=true;
	}

	public void dano() {//funci�n de da�o del enemigo, aqu� ir� el c�digo de lo que le ocurre cuando recibe de�ao,
						//podr�a tener alg�n parametro de entrada, como la cantidad de da�o que se le ha inflingido
		
		
		this.vivo=false; //yo he decidido que mis enemigos mueran en cuanto reciban da�o la primera vez
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		// TODO Auto-generated method stub
		if(vivo)super.draw(batch, parentAlpha);//solo lo pinto si est� vivo
	}
	
	
}
