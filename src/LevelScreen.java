import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

public class LevelScreen extends BaseScreen{
//Clase que carga y gestiona todos los elementos de un nivel
	public Nave jugador;//personaje jugador (antes era un koala)
	
	//vectores con los elementos del juego, tiene que haber un vector para cada tipo distinto de elemento
	private ArrayList<Solid> solidos;
	private ArrayList<Gas> gases;
	public ArrayList<Enemigo> enemigos;
	public ArrayList<Barrera> barreras;
	public ArrayList<Plataforma> plataformas;
	//Variables para el audio
	private Music theme;
	private Sound jump;
	private float volumen;
	//Etiquetas para la interfaz
	private Label gasolinaLabel;
	private Label cascoRestante;
	private Label velocidad;
	private Label puntuacion;
	private Boton boton;
	
	
	@Override
	public void initialize() {
		// TODO Auto-generated method stub
		
		//Creaci�n del fondo
		Fondo fondo=new Fondo(0,0,backStage, this);
		//Ajuste de la c�mara para que muestre la cantidad de pixels que deseamos
		((OrthographicCamera)this.mainStage.getCamera()).setToOrtho(false, Parametros.anchura*3, Parametros.altura*3);
		
		//El tilemapActor se encarga de cargar un mapa creado en Tiled. Cargo el mapa correspondiente seg�n el nivel que toque
		TilemapActor tma;
		switch(Parametros.nivel) {
		case 1: tma = new TilemapActor("assets/mapas/mapa.tmx",mainStage); break;
		case 2:  tma = new TilemapActor("assets/mapas/mapa.tmx",mainStage); break;
		default: tma = new TilemapActor("assets/mapas/mapa.tmx",mainStage); break;
		}
		
		//Obtengo el punto de inicio que he marcado en el mapa, con el valor 'start' en el atributo name
		MapObject startPoint=tma.getRectangleList("start").get(0);
		MapProperties startProps=startPoint.getProperties();
		//Utilizo las propiedades del punto cargado para darle la posici�n inicial al jugador cuando lo creo
		jugador= new Nave((float)startProps.get("x"), (float)startProps.get("y"), mainStage,this);
		
		//Inicializo los vectores que contendr�n los elementos de juego
		solidos= new ArrayList<Solid>();
		gases=new ArrayList<Gas>();
		enemigos= new ArrayList<Enemigo>();
		barreras= new ArrayList<Barrera>();
		plataformas=new ArrayList<Plataforma>();
		//Los elementos siempre se cargar�n con un bucle que obtendr� del tma los objetos almacenados con el valor
		//deseado del atributo name
		
		/*Solidos*/
		Suelo suelo;
		MapProperties props;
		
		for(MapObject obj: tma.getRectangleList("Solido")) {
			props=obj.getProperties();
			suelo=new Suelo((float)props.get("x"),(float)props.get("y"),
					(float)props.get("width"),(float)props.get("height"),mainStage);
			solidos.add(suelo);
		}
		
		//Cajas de gasolina
		Gas gas;
		for(MapObject obj:tma.getTileList("gas")) {
			props=obj.getProperties();
			gas=new Gas((float)props.get("x"),(float)props.get("y"),mainStage);
			gases.add(gas);
		}
		
		//Plataformas
		Plataforma plataforma;
		for(MapObject obj:tma.getTileList("plataforma")) {
			props=obj.getProperties();
			plataforma=new Plataforma((float)props.get("x"),(float)props.get("y"),mainStage);
			plataformas.add(plataforma);
		}
		
		//candados
		Barrera barrera;
		for(MapObject obj:tma.getTileList("barrera")) {
			props=obj.getProperties();
			barrera=new Barrera((float)props.get("x"),(float)props.get("y"),mainStage);
barreras.add(barrera);
			solidos.add(barrera);
		}
		//Bot�n solo hay uno, as� que no hace falta bucle
		MapObject botonM= tma.getTileList("boton").get(0);        
       props = botonM.getProperties();      
       boton= new Boton( (float)props.get("x"), (float)props.get("y"), mainStage );
	
		
		//Moscas
		Enemigo enemigo;
		for (MapObject obj: tma.getTileList("mosca")) {
			
			props=obj.getProperties();
			enemigo=new Mosca((float)props.get("x"),(float)props.get("y"),mainStage,this);
			enemigos.add(enemigo);
		}
		
		
		
		//Cargo los audios del nivel
		theme=Gdx.audio.newMusic(Gdx.files.internal("assets/musica.mp3"));
		jump=Gdx.audio.newSound(Gdx.files.internal("assets/jump.wav"));
		
		volumen=0.6f;
		
		theme.setLooping(true);
		theme.setVolume(volumen);
		theme.play();
		
		//Creaci�n de la interfaz
		velocidad=new Label("Velocidad: ", BaseGame.labelStyle);
		this.gasolinaLabel=new Label("Gasolina: ", BaseGame.labelStyle);
		gasolinaLabel.setColor(Color.RED);
		this.cascoRestante=new Label("Casco: ", BaseGame.labelStyle);
		cascoRestante.setColor(Color.GRAY);
		this.puntuacion=new Label("Puntuaci�n: ", BaseGame.labelStyle);
		Label espacio=new Label(" ",BaseGame.labelStyle);
		//los elementos de la interfaz de a�aden a una tabla para que queden ordenados
		uiTable.add(gasolinaLabel).left();
		uiTable.add(puntuacion).right().expandX();
		uiTable.row();
		uiTable.add(cascoRestante).left();
		
		uiTable.row();
		uiTable.add(velocidad);
		uiTable.row();
		uiTable.add(espacio).expandY();
		
		
	}
	//El m�todo update se llamar� una vez en cada iteraci�n del bucle principal. En �l tenemos que actualizar el estado
	//de cada elemento de juego en el frame actual.
	//el par�metro dt indicar� cu�nto tiempo ha pasado entre el frame anterior y el actual, tendremos que utilizarlo
	//en todos aquellos c�lculos en los que intervenga el tiempo
	@Override
	public void update(float dt) {
		// TODO Auto-generated method stub
		
		//En este bucle impedimos que el jugador pueda atravesar enemigos. Con esta configuraci�n
		//los enemigos 'empujan' al jugador
		for(Enemigo enemigo:enemigos) {
			if(enemigo.vivo && jugador.overlaps(enemigo)) {
				Vector2 offset=jugador.preventOverlap(enemigo);//enemigo empuja a jugador, si quisieramos jugador empuja a enemigo pondr�amos enemigo.preventOverlaps(jugador)
				if(Math.abs(offset.x)> Math.abs(offset.y)) {
					jugador.velocityVec.x=0;
				}
				else
					jugador.velocityVec.y=0;
				
			
			}
		}
		
		//En este bucle impedimos que el jugador pueda atravesar el suelo
		for(Solid solido : solidos) {
			
			if(solido.isEnabled() && jugador.overlaps(solido)) {
				Vector2 offset=jugador.preventOverlap(solido);
				
				jugador.colision();
				
				if(Math.abs(offset.x)> Math.abs(offset.y)) {
					jugador.velocityVec.x=0;
				}
				else
					jugador.velocityVec.y=0;
				
			}
			
		}
		
		for(Enemigo enemigo:enemigos) {
			if(enemigo.vivo && jugador.overlaps(enemigo)) {
				Vector2 offset=enemigo.preventOverlap(jugador);
				if(Math.abs(offset.x)> Math.abs(offset.y)) {
					jugador.velocityVec.x=0;
				}
				else
					jugador.velocityVec.y=0;
				
			
			}
		}
		
		//Con este bucle controlamos la interacci�n(colisi�n) del jugador con las cajas de gasolina y decimos qu� ocurre
		//cuando colisiona con una
		for(Gas gas:gases) {
			if(gas.enabled && jugador.overlaps(gas)) {
				jugador.setGasolina(jugador.getGasolina()+50);
				gas.enabled=false;
			}
		}
		//Si el jugador toca el boton
		if(jugador.overlaps(boton) && !boton.pulsado) {
			boton.pulsar();
			for(Barrera barrera : barreras) {//desactivamos barreras
				barrera.setEnabled(false);
				
			}
		}
	
		
		if(jugador.isOnPlatform() && jugador.velocityVec.len()<jugador.velocidadColision) {//si el jugador est� sobre la plataforma y no lleva velocidad de colision
		
			Parametros.fin=false; //el fin de nivel no es por fin de juego
			Parametros.nivel+=1;//siguiente nivel
			gameOver();//pantalla de fin
		}
		if(jugador.casco<=0 || (jugador.getGasolina()<=0 && jugador.isOnSolid())) {//si el jugador muere por falta de casco o por tocar suelo y no tener gasolina
			
			Parametros.fin=true;
			Parametros.nivel=1;
			this.gameOver();
		}
		//Al final del m�todo actualizamos los elementos de la interfaz
		this.gasolinaLabel.setText("Gas: "+jugador.getGasolina());
		this.cascoRestante.setText("Casco: "+(int)jugador.casco);
		if(Math.abs(jugador.velocityVec.len())<jugador.velocidadColision/3) {
			this.velocidad.setColor(Color.GREEN);
		} else if(Math.abs(jugador.velocityVec.len())<jugador.velocidadColision*3/5) {
			this.velocidad.setColor(Color.YELLOW);
		} else if(Math.abs(jugador.velocityVec.len())<jugador.velocidadColision*4/5) {
			this.velocidad.setColor(Color.ORANGE);
		} else {
			this.velocidad.setColor(Color.RED);
		}
			this.velocidad.setText("Velocidad: "+(int)jugador.velocityVec.len());
	}

	public ArrayList<Solid> getSolidos(){
		return this.solidos;
	}
	
	public void JumpSound() {
		//hace sonar el sonido de salto
		jump.play(volumen);
	}
	public void gameOver() {//m�todo que termina el nivel y da paso a la pantalla de victoria/game over
		this.theme.dispose();
		this.jump.dispose();
		this.dispose();
		
		BaseGame.setActiveScreen(new GameOverScreen());
	}
	
}
