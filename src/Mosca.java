import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class Mosca extends Enemigo{
//Enemigo que persigue al jugador cuando este se acerca y le dispara
	Animation andando;

	boolean persiguiendo;//flag que indica si est� persiguiendo al jugador
	
	
	private float tiempoComportamiento;
	private float cuentaComportamiento;
	
	private float aceleracion;
	private float velocidadMaxima;
	private int numero;
	private float distanciaVision;
	private float inicioX;
	private float inicioY;
	
	
	private float tiempoDisparo;
	private float cuentaDisparo;
	private ArrayList<BalaMosca> balas;
	private int numbalas;
	private int balaActual;

	public Mosca(float x, float y, Stage s, LevelScreen nivel) {

		super(x, y, s, nivel);
		// TODO Auto-generated constructor stub
		this.inicioX=x;
		this.inicioY=y;
		persiguiendo = false;
		String[] walkFileNames = { "assets/enemies/fly_dead.png", "assets/enemies/fly.png" };
		andando = loadAnimationFromFiles(walkFileNames, 0.2f, true);
		velocidadMaxima = 700;
		aceleracion = 900;
		this.cuentaComportamiento=0;
		this.tiempoComportamiento=0.5f;
		distanciaVision=400;
		numbalas=5;
		balaActual=0;
		balas=new ArrayList<BalaMosca>();
		BalaMosca bala;
		this.tiempoDisparo=1;
		this.cuentaDisparo=0;
		for(int i=0; i<numbalas; i++) {
			bala=new BalaMosca(0,0,s,nivel);
			balas.add(bala);
		}
	}

	@Override
	public void act(float dt) {
		// TODO Auto-generated method stub
		super.act(dt);
		if (this.vivo) {
			if (this.persiguiendo) {
				perseguir(nivel.jugador.getX(),nivel.jugador.getY(), false);
				if(this.cuentaDisparo<=0) {
					disparar();
				}else {
					cuentaDisparo-=dt;
				}
			} else {
				if (Math.sqrt(Math.pow(this.getX() - nivel.jugador.getX(), 2)
						+ Math.pow(this.getY() - nivel.jugador.getY(), 2)) < distanciaVision) {
					persiguiendo = true;
				} else {
					if(this.cuentaComportamiento<=0) {
					numero =(int) Math.floor(Math.random()*6+1);
					cuentaComportamiento=this.tiempoComportamiento;
					}
					else {cuentaComportamiento-=dt;}
					switch(numero){
						case 1: accelerationVec.add(-aceleracion, 0);
							break;
						case 2: accelerationVec.add(aceleracion, 0);
							break;
						case 3: accelerationVec.add(0,-aceleracion);
							break;
						case 4: accelerationVec.add(0,aceleracion);
							break;
						case 5: velocityVec.set(0,0);
							this.cuentaComportamiento=0f;
						break;
						case 6: perseguir(inicioX,inicioY, true);
						break;
					
					}
				}
			}

			velocityVec.add(accelerationVec.x * dt, accelerationVec.y * dt);
			velocityVec.x = MathUtils.clamp(velocityVec.x, -velocidadMaxima, velocidadMaxima);
			velocityVec.y = MathUtils.clamp(velocityVec.y, -velocidadMaxima, velocidadMaxima);

			accelerationVec.set(0, 0);
			moveBy(velocityVec.x * dt, velocityVec.y * dt);
		}
	}

	
	private void perseguir(float x, float y, boolean accurate) {
		float margen=0;
		if(!accurate) {
			margen=400;
		}
		
		if (x+margen < this.getX()) {
			accelerationVec.add(-aceleracion, 0);

		} else if (x - margen > this.getX()) {
			accelerationVec.add(aceleracion, 0);
		}
		if (y + margen< this.getY()) {
			accelerationVec.add(0, -aceleracion);

		} else if (y - margen> this.getY()) {
			accelerationVec.add(0, aceleracion);
		}
	}
	
	private void disparar() {
		this.cuentaDisparo=this.tiempoDisparo;
		
		float x=this.getX()-nivel.jugador.getX();
		float y=this.getY()-nivel.jugador.getY();
		Vector2 vector=new Vector2(x,y).nor();
		balas.get(balaActual).disparar(vector.x*-1, vector.y*-1, this.getX(),this.getY());
		balaActual++;
		balaActual=balaActual%numbalas;
		
		
	}
}
