import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class Nave extends BaseActor{
//Personaje principal del juego
	
	//Posibles animaciones
	private Animation parado;
	private Animation andando;
	private Animation saltando;
	
	//atributos para la gesti�n del disparo
	private ArrayList<Bala> balas; //vector de balas, las balas se reutilizan, en vez de crearse en cada disparo (lo que se llama un 'pool' de balas)
	private int numBalas;//n�mero total de balas del vector
	private float coolDownBala;//tiempo que tiene que transcurrir desde un disparo hasta que se pueda volver a disparar
	private float cuentaCoolDownBala;//variable para llevar la cuenta del cooldown anterior
	private int balaActual;//'puntero' a la pr�xima bala que ser� disparada
	
	//atributos para el da�o y da�o seg�n velocidad
	public float velocidadColision;//velocidad a partir de la cual la colisi�n producir� da�o
	public float casco;//'vida' actual
	
	//atributos para gestionar el movimiento 'fluido'
	private float aceleracionAndando;
	private float deceleracionAndando;
	private float velocidadMaximaAndando;
	private float velocidadSalto;
	
	
	//atributos para la gesti�n de la gasolina y el consumo
	private float gasolina;//gasolina actual
	private float consumo; //consumo por unidad de tiempo
	private float cuentaConsumo;//variable para llevar la cuenta de cu�ndo toca consumir
	private float tiempoConsumo;//'unidad de tiempo' cada cuanto se consume
	private boolean propulsor; //flag que indica si alg�n propulsor est� encendido y por lo tanto hay que contar el tiempo de consumo
	
	//gesti�n de invulnerabilidad
	private float tiempoInvulnerable; //tiempo que permanece invulnerable
	private float cuentaInvulnerable;//cuenta cuanto tiempo lleva invulnerable
	
	//referencia al nivel actual para poder acceder a sus elementos
	private LevelScreen nivel;
	
	//Sensor para el salto
	private float alturaPies;
	private float offsetPies;
	private BaseActor pies;//sensor de los pies
	private ShapeRenderer shapeRenderer;//encargado de pintar la caja de colisi�n (quitar en la versi�n final)
	
	
	public Nave(float x, float y, Stage s, LevelScreen nivel) {
		super(x, y, s);
		// TODO Auto-generated constructor stub
		this.nivel=nivel;
		//cargar la animaci�n del personaje quieto, en este caso compuesta por una �nica imagen
		parado=loadTexture("assets/player/alienGreen_stand.png");
		//cargar la animaci�n de andar
		String[] walkFileNames= {"assets/player/alienGreen_walk1.png","assets/player/alienGreen_walk2.png"};
		andando=loadAnimationFromFiles(walkFileNames, 0.2f, true);//imagenes, duraci�n de frame, si es animaci�n en bucle
		//cargar la animaci�n de salto
		saltando=loadTexture("assets/player/alienGreen_jump.png");
		//Creaci�n del pol�gono de colisi�n del personaje
		setBoundaryPolygon(10);
		
		//Valor de los distintos atributos
		velocidadMaximaAndando=900;
		aceleracionAndando=400;
		deceleracionAndando=300;
		velocidadSalto=450;
		alturaPies=6;
		offsetPies=10;
		gasolina=200;
		consumo=5;
		tiempoConsumo=0.3f;
		cuentaConsumo=0;
		propulsor=false;
		velocidadColision=600;
		casco=100;
		tiempoInvulnerable=1;
		cuentaInvulnerable=0;
		
		balaActual=0;
		numBalas=30;
		this.coolDownBala=0.5f;
		this.cuentaCoolDownBala=0;
		//Inicializaci�n del vector de balas
		balas=new ArrayList<Bala>();
		//creaci�n de las balas
		for(int i=0; i<numBalas; i++) {
			balas.add(new Bala(0,0,s,nivel));
		}
		
		shapeRenderer = new ShapeRenderer();
		//Creaci�n del sensor de salto
		pies = new BaseActor(0,0,s);
		pies.loadTexture("assets/white.png");
		pies.setSize(this.getWidth()-offsetPies, alturaPies);
		pies.setBoundaryRectangle();
		
		
		
		
	}
	
	public void act(float dt) {
		
		super.act(dt);
		//por defecto no se est� usando el propulsor
		propulsor=false;
		
		if(this.cuentaInvulnerable>0) {//si el personaje es invulnerable, hay que descontar tiempo de invulnerabilidad
			this.cuentaInvulnerable-=dt;
		}
		if(gasolina>0) {//solo si tiene gasolina podr� moverse, si se mueve propulsor valdr� 'true'
		if((Gdx.input.isKeyPressed(Keys.W) || Gdx.input.isKeyPressed(Keys.UP)) ){
			accelerationVec.add(0, aceleracionAndando*3);
			propulsor=true;
			
		}
		if(Gdx.input.isKeyPressed(Keys.S) || Gdx.input.isKeyPressed(Keys.DOWN)){
			accelerationVec.add(0, -aceleracionAndando);
			propulsor=true;
		}
		if(Gdx.input.isKeyPressed(Keys.A) || Gdx.input.isKeyPressed(Keys.LEFT)){
			accelerationVec.add(-aceleracionAndando,0 );
			propulsor=true;
		}
		if(Gdx.input.isKeyPressed(Keys.D) || Gdx.input.isKeyPressed(Keys.RIGHT)){
			accelerationVec.add(aceleracionAndando, 0);
			propulsor=true;
		}
		}
		//disparo
		if(Gdx.input.isKeyPressed(Keys.E) && this.cuentaCoolDownBala<=0) {
			this.disparar(1);
		}
		
		if(Gdx.input.isKeyPressed(Keys.Q) && this.cuentaCoolDownBala<=0) {
			this.disparar(-1);
		}
		
		if(cuentaCoolDownBala>0) {//Si a�n no se ha terminado el tiempo de coolDown de disparo, se decrementa
			this.cuentaCoolDownBala-=dt;
		}
		
		//salto
		if(Gdx.input.isKeyPressed(Keys.SPACE)) {
			if(this.isOnSolid()) {//solo se puede saltar si el personaje est� sobre un solido
			this.salto();}
						
		}
		
		//Si el propulsor est� encendido
		if(propulsor==true) {
			cuentaConsumo+=dt;
			if(cuentaConsumo>=tiempoConsumo) {//Si se ha alcanzado el tiempo de consumo
				cuentaConsumo=0;//Se reinicia el contador
				gasolina-=consumo;//se consume gasolina
			}
		}
		
		
		if(!Gdx.input.isKeyPressed(Keys.A) && !Gdx.input.isKeyPressed(Keys.D) && !Gdx.input.isKeyPressed(Keys.LEFT) && !Gdx.input.isKeyPressed(Keys.RIGHT))
		{//Si no hay ninguna tecla de movimiento pulsada hay que decelerar
			float cantidadDecelerando=deceleracionAndando*dt;
			float direccionAndando;
			if(velocityVec.x>0)
				direccionAndando=1;
			else
				direccionAndando=-1;
			
			float velocidadAndando=Math.abs(velocityVec.x);
			velocidadAndando-=cantidadDecelerando;
			if(velocidadAndando<0)
				velocidadAndando=0;
			velocityVec.x=velocidadAndando*direccionAndando;
			
		}
		
		//Una vez aplicadas todas las fuerzas propuestas por el jugador, aplicamos la gravedad al vector de aceleraci�n
		accelerationVec.add(0,-Parametros.getGravedad());
		
		//Calculamos la velocidad a partir de la aceleraci�n
		velocityVec.add(accelerationVec.x*dt, accelerationVec.y*dt);
		velocityVec.x=MathUtils.clamp(velocityVec.x, -velocidadMaximaAndando, velocidadMaximaAndando);
		
		//Calculamos el movimiento a partir de la velocidad
		moveBy(velocityVec.x*dt, velocityVec.y*dt);
		//reiniciamos la aceleraci�n (la velocidad se mantiene entre iteraciones, la aceleraci�n no.)
		accelerationVec.set(0,0);
		
		//actualizamos la posici�n del sensor de los pies
		pies.setPosition(this.getX()+offsetPies/2, this.getY()-alturaPies);
		
		//gestionamos la animaci�n actual seg�n el estado del personaje
		if(this.isOnSolid()) {
			if(velocityVec.x==0) {
				setAnimation(parado);
			}
			else {
				setAnimation(andando);
			}
			
		}
		else {
			setAnimation(saltando);
		}
		
		//Consultamos la velocidad en el eje x para saber si tenermos que pintar al personaje mirando hacia la derecha o hacia la izquierda
		if(velocityVec.x>0) {
			setScaleX(1);
		} else if(velocityVec.x<0) {
			setScaleX(-1);
		}
		
		//alinear la c�mara con el personaje
		alignCamera();
		//impedir que el personaje se salga del mundo
		boundToWorld();
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		//Sobreescribir el draw solo es necesario para poder ver la caja de colisi�n
		// TODO Auto-generated method stub
		super.draw(batch, parentAlpha);
		batch.end();
		shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());
		shapeRenderer.begin(ShapeType.Line);
		shapeRenderer.setColor(Color.WHITE);
		if(this.getBoundaryPolygon()!=null) {
		float[] vertices =	new float[this.getBoundaryPolygon().getVertices().length];
				
		for(int i=0;i<vertices.length/2;i++) {
			vertices[2*i]=this.getBoundaryPolygon().getVertices()[2*i]+this.getX();
			vertices[2*i+1]=this.getBoundaryPolygon().getVertices()[2*i+1]+this.getY();
			
		}
		
		shapeRenderer.polygon(vertices);
		}
		shapeRenderer.end();	
		batch.begin();
		
		
		
	}

	public boolean isOnSolid() {
		//devuelve 'true' si el personaje est� sobre un s�lido activo
		for(Solid solido : nivel.getSolidos()) {
			if(this.pies.overlaps(solido) && solido.isEnabled()) {
				return true;
			}
		
		}
		return false;
		
	}
	
	public void salto() {
		//funci�n de salto (modifica la velocidad en y)
		velocityVec.y=velocidadSalto;
		nivel.JumpSound();
	}
	public float getGasolina() {
		return gasolina;
	}

	public void setGasolina(float gasolina) {
		this.gasolina = gasolina;
	}

	public void disparar(int direccion) {
		//funci�n que dispara la bala actual y actualiza las variables relacionadas
		balas.get(balaActual).disparar(direccion);
		balaActual++;
		balaActual=balaActual%numBalas;
		cuentaCoolDownBala=coolDownBala;
	}
	public void colision() {
		//Funci�n que aplica el da�o por colisi�n seg�n la velocidad y si no se es invulnerable
		if(Math.abs(velocityVec.len())>velocidadColision && this.cuentaInvulnerable<=0) {
			casco-=Math.abs(velocityVec.len())*10/velocidadColision;
			this.cuentaInvulnerable=this.tiempoInvulnerable;
		}
		
		
	}
	public void dano(int dano) {//funci�n para hacer una cantidad concreta de da�o al personaje
		if(this.cuentaInvulnerable<=0) {
			this.casco-=dano;
			this.cuentaInvulnerable=this.tiempoInvulnerable;
		}
		
		
	}
	
	
	public boolean isOnPlatform() {
		for(Plataforma solido : nivel.plataformas) {
			if(this.pies.overlaps(solido) ) {
				return true;
			}
		
		}
		return false;
		
	}
	
}
