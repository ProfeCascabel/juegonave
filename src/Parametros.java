
public class Parametros {
	//Parámetros globlales accesibles para todas las clases
 private static float gravedad=750;
 public static int anchura=1200;
 public static int altura=900;
 public static int nivel=1;
 public static boolean fin=true; //si el juego acaba y fin= true significa que ha terminado por muerte

public static float getGravedad() {
	return gravedad;
}

public static void setGravedad(float gravedad) {
	Parametros.gravedad = gravedad;
}
 
}
