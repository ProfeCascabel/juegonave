import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class Suelo extends Solid{
	ShapeRenderer shapeRenderer;
	
	
	public Suelo(float x, float y, float width, float height, Stage s) {
		super(x, y, width, height, s);
		// TODO Auto-generated constructor stub
	
		shapeRenderer = new ShapeRenderer();
	}


	@Override
	public void draw(Batch batch, float parentAlpha) {
		// TODO Auto-generated method stub
		//super.draw(batch, parentAlpha);
		batch.end();
		shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());
		shapeRenderer.begin(ShapeType.Line);
		shapeRenderer.setColor(Color.WHITE);
	
		
		shapeRenderer.rect(this.getX(), this.getY(),this.getWidth(), this.getHeight());
		
		shapeRenderer.end();	
		batch.begin();
	}

}
