import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputEvent.Type;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class TitleScreen extends BaseScreen{
private Table table;
	@Override
	public void initialize() {
		// TODO Auto-generated method stub
		table=new Table();
		table.setFillParent(true);
		//Gdx.input.setInputProcessor(this.uiStage);
		this.uiStage.addActor(table);
		
		TextButton boton=new TextButton("Jugar", BaseGame.textButtonStyle);
		boton.addListener(
				(Event e)->{if(!(e instanceof InputEvent)|| !((InputEvent)e).getType().equals(Type.touchDown))
					return false;
				this.dispose();
				BaseGame.setActiveScreen(new LevelScreen());
				return false;
				});
		table.add(boton);
		
		TextButton botonOpciones=new TextButton("Jugar", BaseGame.textButtonStyle);
		
		table.add(botonOpciones);
		table.row();
		
		TextButton botonSalir=new TextButton("Salir", BaseGame.textButtonStyle);
		botonSalir.addListener(
				(Event e)->{if(!(e instanceof InputEvent)|| !((InputEvent)e).getType().equals(Type.touchDown))
					return false;
				this.dispose();
				Gdx.app.exit();
				return false;
				});
		table.add(botonSalir);
		
	}

	@Override
	public void update(float dt) {
		// TODO Auto-generated method stub
		
	}
	

}
